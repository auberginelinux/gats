G🍆TS Package manager.

I don't know what to write here so I'm putting down important 
rules for particular wording the program likes to see. First
is for architecture. Below is a list of the proper terms for
each architecture.

x86, x86_64, arm, arm64, mips, mips64, ppc, ppc64

If you use other terminology (such as x32 instead of x86)
your program won't be recognized by gats. Next I'll write
license works that should be used.

BSD, AGPLV3, GPLV3, LGPLV3, AGPLV2, GPLV2, LGPLV2, MIT,
WTFPLV1, WTFPLV2, ECLV2, ECLV1, CPL, APSLV2, APSLV1, OEM

If you use other terminology (such as GPL2 or GPLv2 instead
of GPLV2) your program won't be recognized by gats.

That's it for now. More on the readme soon.

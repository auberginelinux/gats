all:
	@echo Run \`make install\` to install GATS.

install:
	@mkdir -vp $(DESTDIR)/etc/gats
	@mkdir -vp $(DESTDIR)/etc/gats/version.ctrl.d
	@cp -vp gats.conf $(DESTDIR)/etc/gats
	@cp -vp base.nfo $(DESTDIR)/etc/gats
	@cp -vp version.ctrl.d/* $(DESTDIR)/etc/gats/version.ctrl.d
	@cp -vp gsync $(DESTDIR)/usr/bin/
	@chmod 755 $(DESTDIR)/usr/bin/gsync
	@cp -vp gadd $(DESTDIR)/usr/bin/
	@chmod 755 $(DESTDIR)/usr/bin/gadd

uninstall:
	@rm -rf /etc/gats
	@rm -rf $(DESTDIR)/usr/bin/gsync
	@rm -rf $(DESTDIR)/usr/bin/gadd


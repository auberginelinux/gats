#!/bin/bash

#grab variables from gats.conf, which, if not present in its proper directory, should
#be downloaded to tmp and used from there for initialization
if [ ! -e /etc/gats/gats.conf ]; then
	echo "WARNING! NO CONFIGURATION FILE WAS FOUND!"
	echo "Downloading new configuration file..."
	mkdir -v /etc/gats
	curl --url https://gitlab.com/auberginelinux/gats/raw/master/gats.conf --output /etc/gats/gats.conf
fi
source /etc/gats/gats.conf

#check if the gats /etc (configuration) directory exists
#if it does not exist, create it and download default files
if [ ! -e $PKGRFIL ] && [ ! -e $VERCDIR ]; then
	cat<<EOF
	WARNING! /ETC FILE TREE ISN'T PRESENT!
	Building /etc file tree...
EOF
	mkdir -v $VERCDIR
	echo Downloading configuration files...
	curl --url $PKGR --output "$PKGRFIL"
fi

#check if the gats /usr (repository info) directory exists
#if it doesn't, create it and download default files
if [ ! -e $MAINDIR ]; then
	cat<<EOF
	WARNING! /USR FILE TREE ISN'T PRESENT!
	Building /usr file tree...
EOF
	mkdir -v $MAINDIR
	echo Cloning initial repository...
	git clone $REPO $REPODIR
fi

if [ -f $CONFFIL ] && [ -f $PKGRFIL ] && [ -d $REPODIR ]; then
echo Cleaning current repository...
rm -rf $REPODIR
echo Done!
echo Cloning latest repository...
git clone $REPO $REPODIR
fi
cat<<EOF
=========================================================================================
Repository updated! Run the "update" command to get the latest versions of your software!
=========================================================================================
EOF
